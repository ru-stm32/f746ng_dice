#include <stdarg.h>
#include <stdio.h>
#include <stddef.h>
#include "myGFX.h"
#include "ILI9341_driver.h"
#include "math.h"

#define swap(a, b) {int16_t t = a; a = b; b = t;}
#define pgm_read_byte(addr) (*(const unsigned char *)(addr))

static uint8_t rotation = 0, textsize, wrap = 1;
static uint16_t _width, _height, cursor_x, cursor_y, textcolor, textbgcolor;

void lcd_init(void) {
	ILI9341_init();

	rotation = 0;
	cursor_y = cursor_x = 0;
	textsize = 1;
	textcolor = 0xFFFF;
	_width = LCD_WIDTH;
	_height = LCD_HEIGHT;
}

void lcd_setRotation(uint8_t r) {
	rotation = (r & 3);
	//hardware set rotation functjion add here;
	ILI9341_setRotation(r);

	if (rotation == 0 || rotation == 2) {
		_width = LCD_WIDTH;
		_height = LCD_HEIGHT;
	} else {
		_width = LCD_HEIGHT;
		_height = LCD_WIDTH;
	}
}

void lcd_drawPixel(int16_t x, int16_t y, uint16_t color) {
	ILI9341_drawPixel(x, y, color);
}

void lcd_drawLineH(uint16_t x0, uint16_t y0, uint16_t l, uint16_t color) {
	for (uint16_t i = x0; i <= (x0 + l) && i < _width; i++)
		lcd_drawPixel(i, y0, color);
}
void lcd_drawLineV(uint16_t x0, uint16_t y0, uint16_t h, uint16_t color) {
	for (uint16_t i = y0; i <= (y0 + h) && i < _height; i++)
		lcd_drawPixel(x0, i, color);
}

void lcd_drawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1,
		uint16_t color) {
	int16_t steep = fabs(y1 - y0) > fabs(x1 - x0);
	if (steep) {
		swap(x0, y0);
		swap(x1, y1);
	}
	if (x0 > x1) {
		swap(x0, x1);
		swap(y0, y1);
	}
	int16_t dx, dy;
	dx = x1 - x0;
	dy = fabs(y1 - y0);
	int16_t err = dx / 2;
	int16_t ystep;
	if (y0 < y1) {
		ystep = 1;
	} else {
		ystep = -1;
	}
	for (; x0 <= x1; x0++) {
		if (steep) {
			lcd_drawPixel(y0, x0, color);
		} else {
			lcd_drawPixel(x0, y0, color);
		}
		err -= dy;
		if (err < 0) {
			y0 += ystep;
			err += dx;
		}
	}
}

void lcd_drawRect(uint16_t x0, uint16_t y0, uint16_t w, uint16_t h,
		uint16_t color) {
	lcd_drawLineH(x0, y0, w, color);
	lcd_drawLineH(x0, y0 + h, w, color);
	lcd_drawLineV(x0, y0, h, color);
	lcd_drawLineV(x0 + w, y0, h, color);
}

void lcd_drawFillRect(uint16_t x0, uint16_t y0, uint16_t w, uint16_t h,
		uint16_t color) {
	for (int16_t i = x0; i < x0 + w; i++) {
		lcd_drawLineV(i, y0, h, color);
	}
}

void lcd_fillScreen(uint16_t color) {
	lcd_drawFillRect(0, 0, _width, _height, color);
}

void lcd_drawCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color) {
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	lcd_drawPixel(x0, y0 + r, color);
	lcd_drawPixel(x0, y0 - r, color);
	lcd_drawPixel(x0 + r, y0, color);
	lcd_drawPixel(x0 - r, y0, color);

	while (x < y) {
		if (f >= 0) {
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;

		lcd_drawPixel(x0 + x, y0 + y, color);
		lcd_drawPixel(x0 - x, y0 + y, color);
		lcd_drawPixel(x0 + x, y0 - y, color);
		lcd_drawPixel(x0 - x, y0 - y, color);
		lcd_drawPixel(x0 + y, y0 + x, color);
		lcd_drawPixel(x0 - y, y0 + x, color);
		lcd_drawPixel(x0 + y, y0 - x, color);
		lcd_drawPixel(x0 - y, y0 - x, color);
	}
}

void lcd_drawFillCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color) {
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	lcd_drawLineV(x0, y0 - r, 2 * r, color);
	lcd_drawLineH(x0 - r, y0, 2 * r, color);

	while (x < y) {
		if (f >= 0) {
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;

		lcd_drawLineV(x0 + x, y0 - y, 2 * y, color);
		lcd_drawLineV(x0 - x, y0 - y, 2 * y, color);
		lcd_drawLineH(x0 - y, y0 - x, 2 * y, color);
		lcd_drawLineH(x0 - y, y0 + x, 2 * y, color);
	}
}

static void lcd_drawChar(int16_t x, int16_t y, unsigned char c, uint16_t color,
		uint16_t bg, uint8_t size) {

	if ((x >= _width) || // Clip right
			(y >= _height) || // Clip bottom
			((x + 6 * size - 1) < 0) || // Clip left
			((y + 8 * size - 1) < 0))   // Clip top
		return;

	for (int8_t i = 0; i < 6; i++) {
		uint8_t line;
		if (i == 5)
			line = 0x0;
		else
			line = pgm_read_byte(font + (c * 5) + i);
		for (int8_t j = 0; j < 8; j++) {
			if (line & 0x1) {
				if (size == 1) // default size
					lcd_drawPixel(x + i, y + j, color);
				else {  // big size
					lcd_drawFillRect(x + (i * size), y + (j * size), size, size,
							color);
				}
			} else if (bg != color) {
				if (size == 1) // default size
					lcd_drawPixel(x + i, y + j, bg);
				else {  // big size
					lcd_drawFillRect(x + i * size, y + j * size, size, size,
							bg);
				}
			}
			line >>= 1;
		}
	}
}

void lcd_setTextSize(uint8_t s) {
	textsize = (s > 0) ? s : 1;
}

void lcd_setTextColor(uint16_t color, uint16_t backGround) {
	textcolor = color;
	textbgcolor = backGround;
}

void lcd_setTextWrap(uint8_t w) {
	wrap = w;
}

static void lcd_writeChar(uint8_t c) {
	if (c == '\n') {
		cursor_y += textsize * 8;
		cursor_x = 0;
	} else if (c == '\r') {
		// skip em
	} else {
		lcd_drawChar(cursor_x, cursor_y, c, textcolor, textbgcolor, textsize);
		cursor_x += textsize * 6;
		if (wrap && (cursor_x > (_width - textsize * 6))) {
			cursor_y += textsize * 8;
			cursor_x = 0;
		}
	}
}

static void lcd_writeText(char * text) {
	while (*text)
		lcd_writeChar(*text++);
}

void lcd_setCursor(int16_t x, int16_t y){
	cursor_x = x;
	cursor_y = y;
}

void lcd_printf(char *format, ...) {
	char buf[32];

	va_list args;
	va_start(args, format);
	vsniprintf(buf, 32, format, args);
	va_end(args);

	lcd_writeText(buf);
}
