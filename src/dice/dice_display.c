/*
 * dice_display.c
 *
 *  Created on: 29.12.2016
 *      Author: rafal
 */
#include "lcd/myGFX.h"
#include "stdbool.h"

static const uint8_t radius = 15, space = 35;

static void diceDraw(uint8_t center_x, uint8_t center_y, uint8_t value,
		uint16_t dotColor) {
	switch (value) {
	case 1:
		lcd_drawFillCircle(center_x, center_y, radius, dotColor);
		break;
	case 2:
		lcd_drawFillCircle(center_x + space, center_y + space, radius,
				dotColor);
		lcd_drawFillCircle(center_x - space, center_y - space, radius,
				dotColor);
		break;
	case 3:
		lcd_drawFillCircle(center_x, center_y, radius, dotColor);
		lcd_drawFillCircle(center_x + space, center_y + space, radius,
				dotColor);
		lcd_drawFillCircle(center_x - space, center_y - space, radius,
				dotColor);
		break;
	case 4:
		lcd_drawFillCircle(center_x + space, center_y + space, radius,
				dotColor);
		lcd_drawFillCircle(center_x - space, center_y - space, radius,
				dotColor);
		lcd_drawFillCircle(center_x + space, center_y - space, radius,
				dotColor);
		lcd_drawFillCircle(center_x - space, center_y + space, radius,
				dotColor);
		break;
	case 5:
		lcd_drawFillCircle(center_x, center_y, radius, dotColor);
		lcd_drawFillCircle(center_x + space, center_y + space, radius,
				dotColor);
		lcd_drawFillCircle(center_x - space, center_y - space, radius,
				dotColor);
		lcd_drawFillCircle(center_x + space, center_y - space, radius,
				dotColor);
		lcd_drawFillCircle(center_x - space, center_y + space, radius,
				dotColor);
		break;
	case 6:
		lcd_drawFillCircle(center_x, center_y - space, radius, dotColor);
		lcd_drawFillCircle(center_x, center_y + space, radius, dotColor);
		lcd_drawFillCircle(center_x - space, center_y - space, radius,
				dotColor);
		lcd_drawFillCircle(center_x - space, center_y + space, radius,
				dotColor);
		lcd_drawFillCircle(center_x + space, center_y - space, radius,
				dotColor);
		lcd_drawFillCircle(center_x + space, center_y + space, radius,
				dotColor);
		break;
	default:
		break;
	}
}

void diceDisplay_init(void) {
	lcd_drawFillCircle(50, 50, 10, COLOR_WHITE);
	lcd_drawFillCircle(50, 150, 10, COLOR_WHITE);
	lcd_drawFillCircle(150, 50, 10, COLOR_WHITE);
	lcd_drawFillCircle(150, 150, 10, COLOR_WHITE);
	lcd_drawFillRect(40, 50, 121, 100, COLOR_WHITE);
	lcd_drawFillRect(50, 40, 100, 20, COLOR_WHITE);
	lcd_drawFillRect(50, 140, 100, 20, COLOR_WHITE);

	lcd_drawFillCircle(175, 50, 10, COLOR_WHITE);
	lcd_drawFillCircle(175, 150, 10, COLOR_WHITE);
	lcd_drawFillCircle(275, 50, 10, COLOR_WHITE);
	lcd_drawFillCircle(275, 150, 10, COLOR_WHITE);
	lcd_drawFillRect(165, 50, 121, 100, COLOR_WHITE);
	lcd_drawFillRect(175, 40, 100, 20, COLOR_WHITE);
	lcd_drawFillRect(175, 140, 100, 20, COLOR_WHITE);
}

void diceDisplay_clear(void) {
	diceDraw(100, 100, 6, COLOR_WHITE);
	diceDraw(100, 100, 1, COLOR_WHITE);

	diceDraw(225, 100, 6, COLOR_WHITE);
	diceDraw(225, 100, 1, COLOR_WHITE);
}

void diceDisplay_display(uint8_t value1, uint8_t value2) {
	diceDraw(100, 100, value1, COLOR_BLACK);
	diceDraw(225, 100, value2, COLOR_BLACK);
}
