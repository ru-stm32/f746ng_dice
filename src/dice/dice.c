/*
 * dice.c
 *
 *  Created on: 29.12.2016
 *      Author: rafal
 */
#include "dice.h"
#include "gpio.h"
#include "rcc.h"
#include "rng.h"
#include "lcd/myGFX.h"
#include "dice_stateMachine.h"

void dice_init(void) {

	rcc_usePLL216Mhz();

	rcc_AHB1ENR_set(RCC_AHB1ENR_GPIOIEN);

	gpio_pin_cfg(GPIOI, P11, gpio_mode_in_floating);

	lcd_init();
	lcd_setRotation(3);
	lcd_fillScreen(COLOR_BLACK);

	rng_init();

	dice_initStateMachine();
}

void dice_handle(void){
	dice_handleStateMachine();
}
