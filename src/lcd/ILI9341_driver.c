#include "ILI9341_driver.h"
#include "ILI9341_registers.h"
#include "pin_util.h"

static uint16_t _width, _height;

static void delayTicks(uint32_t ticks) {
	while (ticks--) {
		asm volatile("nop");
	}
}

static void setAddrWindow(int x1, int y1, int x2, int y2) {
	CS_ACTIVE;
	uint32_t t;
	t = x1;
	t <<= 16;
	t |= x2;
	pins_writeRegister32(ILI9341_COLADDRSET, t);
	t = y1;
	t <<= 16;
	t |= y2;
	pins_writeRegister32(ILI9341_PAGEADDRSET, t);
	CS_IDLE;
}

void ILI9341_reset(void) {
	CS_IDLE;
	WR_IDLE;
	RD_IDLE;
	RESET_LOW;
	delayTicks(15000);
	RESET_HIGH;
	// Data transfer sync
	CS_ACTIVE;
	CD_COMMAND;
	pins_write8bits(0x00);
	for (uint8_t i = 0; i < 3; i++)
		WR_STROBE
	;
	// Three extra 0x00s
	CS_IDLE;
}

void ILI9341_init(void) {
	pins_init();
	CS_IDLE; // Set all control bits to idle state
	WR_IDLE;
	RD_IDLE;
	CD_DATA;
	RESET_HIGH;
	pins_setWrite();

	ILI9341_reset();
	delayTicks(100000);

//	driver = ID_9341;
	CS_ACTIVE;
	pins_writeRegister8(ILI9341_SOFTRESET, 0);
	delayTicks(20000);
	pins_writeRegister8(ILI9341_DISPLAYOFF, 0);
	pins_writeRegister8(ILI9341_POWERCONTROL1, 0x23);
	pins_writeRegister8(ILI9341_POWERCONTROL2, 0x10);
	pins_writeRegister16(ILI9341_VCOMCONTROL1, 0x2B2B);
	pins_writeRegister8(ILI9341_VCOMCONTROL2, 0xC0);
	pins_writeRegister8(ILI9341_MEMCONTROL,
	ILI9341_MADCTL_MY | ILI9341_MADCTL_BGR);
	pins_writeRegister8(ILI9341_PIXELFORMAT, 0x55);
	pins_writeRegister16(ILI9341_FRAMECONTROL, 0x001B);

	pins_writeRegister8(ILI9341_ENTRYMODE, 0x07);
	/* writeRegister32(ILI9341_DISPLAYFUNC, 0x0A822700);*/

	pins_writeRegister8(ILI9341_SLEEPOUT, 0);
	delayTicks(600000);
	pins_writeRegister8(ILI9341_DISPLAYON, 0);
	delayTicks(200000);

	_width = TFT_WIDTH;
	_height = TFT_HEIGHT;

	setAddrWindow(0, 0, _width - 1, _height - 1);
}

void ILI9341_drawPixel(int16_t x, int16_t y, uint16_t color) {
	if ((x < 0) || (y < 0) || (x >= _width) || (y >= _height))
		return;

	CS_ACTIVE;

	setAddrWindow(x, y, _width - 1, _height - 1);
	CS_ACTIVE;
	CD_COMMAND;
	pins_write8bits(0x2C);
	CD_DATA;
	pins_write8bits(color >> 8);
	pins_write8bits(color);

	CS_IDLE;
}

void ILI9341_setRotation(uint8_t r) {
	uint16_t rotation = (r & 0x03);
	uint16_t t;

	CS_ACTIVE;
	switch (rotation) {
	case 2:
		t = ILI9341_MADCTL_MX | ILI9341_MADCTL_BGR;
		_width = TFT_WIDTH;
		_height = TFT_HEIGHT;
		break;
	case 3:
		t = ILI9341_MADCTL_MV | ILI9341_MADCTL_BGR;
		_width = TFT_HEIGHT;
		_height = TFT_WIDTH;
		break;
	case 0:
		t = ILI9341_MADCTL_MY | ILI9341_MADCTL_BGR;
		_width = TFT_WIDTH;
		_height = TFT_HEIGHT;
		break;
	case 1:
		t = ILI9341_MADCTL_MX | ILI9341_MADCTL_MY | ILI9341_MADCTL_MV
				| ILI9341_MADCTL_BGR;
		_width = TFT_HEIGHT;
		_height = TFT_WIDTH;
		break;
	}
	pins_writeRegister8(ILI9341_MADCTL, t); // MADCTL
	// For 9341, init default full-screen address window:
	setAddrWindow(0, 0, _width - 1, _height - 1); // CS_IDLE happens here
}

