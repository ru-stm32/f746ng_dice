/*
 * dice.h
 *
 *  Created on: 29.12.2016
 *      Author: rafal
 */

#ifndef DICE_DICE_H_
#define DICE_DICE_H_

void dice_init(void);
void dice_handle(void);

#endif /* DICE_DICE_H_ */
